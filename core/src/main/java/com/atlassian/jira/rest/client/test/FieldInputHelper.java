package com.atlassian.jira.rest.client.test;


import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.atlassian.jira.rest.client.api.domain.IssueFieldId;
import com.atlassian.jira.rest.client.api.domain.input.ComplexIssueInputFieldValue;
import com.atlassian.jira.rest.client.api.domain.input.FieldInput;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;

public class FieldInputHelper {

	public static FieldInput toSummary(String summary) {
		return new FieldInput(IssueFieldId.SUMMARY_FIELD, summary);
	}
	
	public static FieldInput toIssueType(Long issueTypeId) {
		return new FieldInput(
				IssueFieldId.ISSUE_TYPE_FIELD,
				ComplexIssueInputFieldValue.with("id", issueTypeId.toString()));
	}
	
	public static FieldInput toPriority(Long priorityId) {
		return new FieldInput(IssueFieldId.PRIORITY_FIELD, ComplexIssueInputFieldValue.with("id", priorityId.toString()));
	}
	
	public static FieldInput toStatus(Long statusId) {
		return new FieldInput(IssueFieldId.STATUS_FIELD, ComplexIssueInputFieldValue.with("id", statusId.toString()));
	}
	
	public static FieldInput toResolution(Long resolutionId) {
		return new FieldInput(IssueFieldId.RESOLUTION_FIELD, ComplexIssueInputFieldValue.with("id", resolutionId));
	}
	
	public static FieldInput toDescription(String description) {
		return new FieldInput(IssueFieldId.DESCRIPTION_FIELD, description);
	}
	
	public static FieldInput toProjectKey(String key) {
		return  new FieldInput(IssueFieldId.PROJECT_FIELD, ComplexIssueInputFieldValue.with("key", key));
	}
	
	public static FieldInput toAssignee(String assignee) {
		return new FieldInput(IssueFieldId.ASSIGNEE_FIELD, ComplexIssueInputFieldValue.with("name", assignee));
	}
	
	public static FieldInput toReporter(String reporter) {
		return new FieldInput(IssueFieldId.REPORTER_FIELD, ComplexIssueInputFieldValue.with("name", reporter));
	}
	
	public static FieldInput toFixVersions(List<String> versions) {
		return new FieldInput(IssueFieldId.FIX_VERSIONS_FIELD, toListOfComplexIssueInputFieldValueWithSingleKey(versions, "name"));
	}
	
	public static FieldInput toAffectVersions(List<String> versions) {
		return new FieldInput(IssueFieldId.AFFECTS_VERSIONS_FIELD, toListOfComplexIssueInputFieldValueWithSingleKey(versions, "name"));
	}
	
	public static FieldInput toComponents(List<String> names) {
		return new FieldInput(IssueFieldId.COMPONENTS_FIELD, toListOfComplexIssueInputFieldValueWithSingleKey(names, "name"));
	}
	
	private static final DateTimeFormatter JIRA_DATE_FORMATTER = ISODateTimeFormat.date();
	private static final DateTimeFormatter JIRA_TIME_FORMATTER = ISODateTimeFormat.dateTimeNoMillis();
	
	public static FieldInput toDueDate(Date date) {
		return new FieldInput(IssueFieldId.DUE_DATE_FIELD, JIRA_DATE_FORMATTER.print(new DateTime(date)));
	}
	
	public static FieldInput toDateField(String field, Date date) {
		return new FieldInput(field, formatDate(date));
	}
	
	public static String formatDate(Date date) {
		return JIRA_DATE_FORMATTER.print(new DateTime(date));
	}
	
	public static String formatTime(Date date) {
		return JIRA_TIME_FORMATTER.print(new DateTime(date));
	}
	
	private static <T> Iterable<ComplexIssueInputFieldValue> toListOfComplexIssueInputFieldValueWithSingleKey(final Iterable<T> items, final String key) {
		return Iterables.transform(items, new Function<T, ComplexIssueInputFieldValue>() {

			@Override
			public ComplexIssueInputFieldValue apply(T value) {
				return ComplexIssueInputFieldValue.with(key, value);
			}
		});
	}
}
