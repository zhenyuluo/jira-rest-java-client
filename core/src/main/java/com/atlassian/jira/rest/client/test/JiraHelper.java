package com.atlassian.jira.rest.client.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class JiraHelper {
	private static Pattern pWorklog = Pattern.compile("^((\\d+w\\s+)?(\\d+d\\s+)?(\\d+h\\s+)?(\\d+m\\s+)?)");
    
    public static String[] parseWorklogText(String text) {
    	if (StringUtils.isBlank(text)) {
    		return new String[0];
    	}
    	
    	Matcher m = pWorklog.matcher(text.trim());
    	
    	if (!m.find()) {
    		return new String[0];
    	}
    	
    	int pos = m.end();
    	String[] result = new String[2];
    	result[0] = text.substring(0, pos).trim();
    	if (pos < text.length())
    		result[1] = text.substring(pos);
    	else
    		result[1] = "";
    	
    	return result;
    }

}
