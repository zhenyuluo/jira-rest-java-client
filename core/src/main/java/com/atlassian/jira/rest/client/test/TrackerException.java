package com.atlassian.jira.rest.client.test;

public class TrackerException extends RuntimeException {

	public TrackerException(String msg, Throwable e) {
		super(msg, e);
	}
	
	public TrackerException(Throwable e) {
		super(e);
	}
	
	public TrackerException(String msg) {
		super(msg);
	}
}
