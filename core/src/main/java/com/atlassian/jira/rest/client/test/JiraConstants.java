package com.atlassian.jira.rest.client.test;

public class JiraConstants {
	// Project Related attributes
	public static final String KEY 							= "key";
	public static final String NAME 						= "name";
	public static final String LEAD							= "lead";
    public static final String DESCRIPTION 					= "description";
	public static final String ISSUE_SECURITY_SCHEME		= "issueSecurityScheme";
	public static final String NOTIFICATION_SCHEME			= "notificationScheme";
	public static final String PERMISSION_SCHEME			= "permissionScheme";
	public static final String URL							= "url";
	
	// Issue related attributes
	public static final String PROJECT 						= "project";
//	public static final String PARENT						= "parent";
	public static final String AFFECTED_VERSIONS 			= "affectsVersions";
	public static final String ASSIGNEE 					= "assignee";
    public static final String COMPONENTS 					= "components";
    public static final String COMMENT 						= "comment";
    public static final String DUEDATE 						= "duedate";
    public static final String DATE_FORMAT					= "dateFormat";
    public static final String ENVIRONMENT 					= "environment";
    public static final String FIX_FOR_VERSIONS 			= "fixVersions";
    public static final String ISSUE_KEY 					= "issuekey";
    public static final String ISSUE_TYPE 					= "type";
//    public static final String THUMBNAIL 					= "thumbnail";
//    public static final String ISSUE_LINKS 				= "issuelinks";
//    public static final String WORKRATIO 					= "workratio";
//    public static final String SUBTASKS 					= "subtasks";
//    public static final String ATTACHMENT 				= "attachment";
    public static final String PRIORITY 					= "priority";
    public static final String REPORTER 					= "reporter";
    public static final String TIME							= "time"; // for worklog
    public static final String SECURITY 					= "security";
    public static final String SUMMARY 						= "summary";
    public static final String CREATED 						= "created";
    public static final String UPDATED 						= "updated";
    public static final String STATUS 						= "status";
    public static final String RESOLUTION 					= "resolution";
    public static final String LABELS                       = "labels";
//    public static final String CUSTOM						= "custom";
    public static final String STEP							= "step";
}