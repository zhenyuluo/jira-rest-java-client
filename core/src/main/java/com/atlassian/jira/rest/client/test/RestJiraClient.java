package com.atlassian.jira.rest.client.test;

import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

import org.apache.commons.lang.StringUtils;
import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.rest.client.api.IdentifiableEntity;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClientFactory;
import com.atlassian.jira.rest.client.api.NamedEntity;
import com.atlassian.jira.rest.client.api.RestClientException;
import com.atlassian.jira.rest.client.api.domain.BasicIssue;
import com.atlassian.jira.rest.client.api.domain.Comment;
import com.atlassian.jira.rest.client.api.domain.Field;
import com.atlassian.jira.rest.client.api.domain.FieldType;
import com.atlassian.jira.rest.client.api.domain.Filter;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.IssueType;
import com.atlassian.jira.rest.client.api.domain.Priority;
import com.atlassian.jira.rest.client.api.domain.Project;
import com.atlassian.jira.rest.client.api.domain.Resolution;
import com.atlassian.jira.rest.client.api.domain.SearchResult;
import com.atlassian.jira.rest.client.api.domain.Status;
import com.atlassian.jira.rest.client.api.domain.Transition;
import com.atlassian.jira.rest.client.api.domain.User;
import com.atlassian.jira.rest.client.api.domain.Version;
import com.atlassian.jira.rest.client.api.domain.input.FieldInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.api.domain.input.TransitionInput;
import com.atlassian.jira.rest.client.api.domain.input.VersionInput;
import com.atlassian.jira.rest.client.api.domain.input.VersionInputBuilder;
import com.atlassian.jira.rest.client.api.domain.input.WorklogInputBuilder;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class RestJiraClient {

	private static final Logger logger = LoggerFactory.getLogger(RestJiraClient.class);
	
	private JiraRestClient restClient;
	
	final JiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
	
	private final String username;
	private final String serverUrl;
	
	public RestJiraClient(String url, String username, String password) {
		URI uri = URI.create(url);
		this.username = username;
		this.serverUrl = url;
		restClient = factory.createWithBasicHttpAuthentication(uri, username, password);
	}
	
	public void open() {
	}
	
	public void close() {
		try {
			restClient.close();
		} catch (IOException e) {
		}
	}
	
	public List<IssueType> getRemoteIssueTypes(String projectKey) {
		Project project = getProject(projectKey);
		return Lists.newArrayList(project.getIssueTypes().iterator());
	}
	
	public List<IssueType> getRemoteIssueTypes() {
		try {
			return Lists.newArrayList(restClient.getMetadataClient().getIssueTypes().claim());
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
	}
	
	public IssueType getRemoteIssueType(String projectKey, String name) {
		return getEntity(getRemoteIssueTypes(projectKey), name);
	}
	
	static List<String> getNames(List<? extends NamedEntity> list) {
		List<String> names = Lists.newArrayList();
		for (NamedEntity each : list) {
			names.add(each.getName());
		}
		
		return names;
	}
	
	public List<Priority> getRemotePriorities() {
		try {
			return Lists.newArrayList(restClient.getMetadataClient().getPriorities().claim());
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
	}
	
	public Priority getRemotePriority(String name) {
		return getEntity(getRemotePriorities(), name);
	}
	
	public List<Resolution> getRemoteResolutions() {
		try {
			return Lists.newArrayList(restClient.getMetadataClient().getResolutions().claim());
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
	}
	
	public Resolution getRemoteResolution(String name) {
		return getEntity(getRemoteResolutions(), name);
	}
	
	public List<Status> getRemoteStatuses() {
		try {
			return Lists.newArrayList(restClient.getMetadataClient().getStatuses().claim());
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
	}
	
	public Status getRemoteStatus(String name) {
		return getEntity(getRemoteStatuses(), name);
	}
	
	public List<Field> getRemoteFields() {
		try {
			return Lists.newArrayList(restClient.getMetadataClient().getFields().claim());
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
	}
	
	public Field getRemoteField(String name) {
		return getEntity(getRemoteFields(), name);
	}
	
	public List<Field> getRemoteCustomFields() {
		return Lists.newArrayList(Iterables.filter(getRemoteFields(), new Predicate<Field>() {

			@Override
			public boolean apply(Field field) {
				return field.getFieldType() == FieldType.CUSTOM;
			}
		}));
	}
	
	public Project getProject(String key) {
		try {
			return restClient.getProjectClient().getProject(key).claim();
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
	}
	
	public List<Filter> getFavouriteFilters() {
		try {
			return Lists.newArrayList(restClient.getSearchClient().getFavouriteFilters().claim());
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
	}
	
	public Filter getFilter(String name) {
		for (Filter each : getFavouriteFilters()) {
			if (each.getName().equalsIgnoreCase(name) || each.getId().toString().equals(name)) {
				return each;
			}
		}
		
		throw new TrackerException("No specified filter " + name + " found.");
	}
	
	private static <T extends NamedEntity> T getEntity(Iterable<T> collection, String name) {
		for (T each : collection) {
			if (each.getName().equalsIgnoreCase(name)) {
				return each;
			}
			
			if (each instanceof IdentifiableEntity) {
				IdentifiableEntity<?> i = (IdentifiableEntity<?>) each;
				if (Objects.equal(i.getId().toString(), name)) {
					return each;
				}
			}
		}
		
		throw new TrackerException(name);
	}
	
	public Issue getRemoteIssue(String issueKey) {
		try {
			return restClient.getIssueClient().getIssue(issueKey).claim();
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
	}
	
	
	public Issue createIssue(final Map<String, String> attributes) {
		IssueInputBuilder builder = new IssueInputBuilder();
		String projectKey = getRequiredString(attributes, JiraConstants.PROJECT);
		if (!attributes.containsKey(JiraConstants.ISSUE_TYPE)) {
			attributes.put(JiraConstants.ISSUE_TYPE, "Bug");
		}
		
		builder.setProjectKey(projectKey);
		for (FieldInput each : mapToFieldInputs(projectKey, attributes)) {
			builder.setFieldInput(each);
		}
		
		IssueInput input = builder.build();
		Promise<BasicIssue> pb = restClient.getIssueClient().createIssue(input);
		BasicIssue bi = null;
		try {
			bi = pb.claim();
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
		
		Issue issue = getRemoteIssue(bi.getKey());
		
		if (attributes.containsKey(JiraConstants.COMMENT)) {
			addComment(issue, getString(attributes, JiraConstants.COMMENT));
		}
		
		if (attributes.containsKey(JiraConstants.TIME)) {
			addWorklog(issue, getString(attributes, JiraConstants.TIME));
		}
		
		return issue;
	}
	
	private static String mergeNames(Iterable<? extends NamedEntity> current, List<String> added) {
		Set<String> names = Sets.newLinkedHashSet();
		for (NamedEntity each : current) {
			names.add(each.getName());
		}
		
		for (String each : added) {
			names.add(each);
		}
		
		return Joiner.on(",").join(names);
	}
	
	public void updateIssue(final String issueKey, final Map<String, String> attributes) {
		Issue issue = getRemoteIssue(issueKey);
		
		if (attributes.containsKey(JiraConstants.AFFECTED_VERSIONS)) {
			String value = mergeNames(issue.getAffectedVersions(), getList(attributes, JiraConstants.AFFECTED_VERSIONS));
			attributes.put(JiraConstants.AFFECTED_VERSIONS, value);
		}
		
		if (attributes.containsKey(JiraConstants.FIX_FOR_VERSIONS)) {
			String versions = mergeNames(issue.getFixVersions(), getList(attributes, JiraConstants.FIX_FOR_VERSIONS));
			attributes.put(JiraConstants.FIX_FOR_VERSIONS, versions);
		}
		
		if (attributes.containsKey(JiraConstants.COMPONENTS)) {
			String components = mergeNames(issue.getComponents(), getList(attributes, JiraConstants.COMPONENTS));
			attributes.put(JiraConstants.COMPONENTS, components);
		}
		
		IssueInputBuilder builder = new IssueInputBuilder();
		for (FieldInput each : mapToFieldInputs(issue.getProject().getKey(), attributes)) {
			builder.setFieldInput(each);
		}
		
		try {
			restClient.getIssueClient().updateIssue(issueKey, builder.build()).claim();
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
		
		if (attributes.containsKey(JiraConstants.COMMENT)) {
			addComment(issue, getString(attributes, JiraConstants.COMMENT));
		}
		
		if (attributes.containsKey(JiraConstants.TIME)) {
			addWorklog(issue, getString(attributes, JiraConstants.TIME));
		}
	}
	
	public void progressWorkflow(final String issueKey, Map<String, String> attributes) {
		Issue issue = getRemoteIssue(issueKey);
		
		String step = getRequiredString(attributes, JiraConstants.STEP);
		Transition found = null;
		
		Iterable<Transition> transitions = Collections.emptyList();
		try {
			transitions = restClient.getIssueClient().getTransitions(issue).claim();
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
		
		for (Transition t : transitions) {
			if (t.getName().equalsIgnoreCase(step) || step.equals(String.valueOf(t.getId()))) {
				found = t;
			}
		}
		
		if (found == null) {
			throw new TrackerException(String.format("Can't progress issue %s with step %s", issueKey, step));
		}
		
		TransitionInput ti = new TransitionInput(found.getId(), 
				mapToFieldInputs(issue.getProject().getKey(), attributes));
		try {
			restClient.getIssueClient().transition(issue, ti).claim();
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}

		if (attributes.containsKey(JiraConstants.COMMENT)) {
			addComment(issue, getString(attributes, JiraConstants.COMMENT));
		}
		
		if (attributes.containsKey(JiraConstants.TIME)) {
			addWorklog(issue, getString(attributes, JiraConstants.TIME));
		}
	}
	
	public void addComment(Issue issue, String comment) {
		try {
			restClient.getIssueClient().addComment(issue.getCommentsUri(), Comment.valueOf(comment)).claim();
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
	}

	public Version createVersion(String projectKey, String name) {
		return createVersion(projectKey, name, null);
	}
	
	public Version createVersion(String projectKey, String name, String description) {
		VersionInputBuilder builder = new VersionInputBuilder(projectKey);
		builder.setName(name);
		
		if (!Strings.isNullOrEmpty(description))
			builder.setDescription(description);
		
		try {
			return restClient.getVersionRestClient().createVersion(builder.build()).claim();
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
	}
	
	public @Nullable Version getVersion(String projectKey, String name) {
		Project project = getProject(projectKey);
		for (Version each : project.getVersions()) {
			if (each.getName().equalsIgnoreCase(name) || each.getId().toString().equalsIgnoreCase(name)) {
				return each;
			}
		}
		
		return null;
	}
	
	public List<String> getAllVersionNames(String projectKey) {
		Project project = getProject(projectKey);
		List<String> names = Lists.newArrayList();
		for (Version each : project.getVersions()) {
			names.add(each.getName());
		}
		
		return names;
	}
	
	public List<String> getUnreleasedVersionNames(String projectKey) {
		Project project = getProject(projectKey);
		List<String> names = Lists.newArrayList();
		for (Version each : project.getVersions()) {
			if (!each.isReleased() && !each.isArchived())
				names.add(each.getName());
		}
		
		return names;
	}
	
	public void releaseVersion(String projectKey, String name) {
		Version v = getVersion(projectKey, name);
		if (v == null) {
			v = createVersion(projectKey, name);
		}
		
		VersionInput input = new VersionInput(projectKey, name, v.getDescription(), new DateTime(), false, true);
		try {
			restClient.getVersionRestClient().updateVersion(v.getSelf(), input).claim();
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
	}
	
	private static Pattern pNumber = Pattern.compile("\\d+");
	
	private int parseMinutes(String text) {
		int minutes = 0;
		
		for (String each : Splitter.on(" ").trimResults().split(text)) {
			Matcher m = pNumber.matcher(each);
			if (m.find()) {
				int n = Integer.valueOf(m.group());
				if (each.endsWith("w")) {
					minutes += n * DateTimeConstants.MINUTES_PER_WEEK;
				} else if (each.endsWith("d")) {
					minutes += n* DateTimeConstants.MINUTES_PER_DAY;
				} else if (each.endsWith("h")) {
					minutes += n * DateTimeConstants.MINUTES_PER_HOUR;
				} else if (each.endsWith("m")) {
					minutes += n;
				}
			}
		}
		
		return minutes;
	}
	
	public void addWorklog(Issue issue, String text) {
		String[] tokens = JiraHelper.parseWorklogText(text);
		
		if (tokens.length == 0) {
			return;
		}
    	
    	int minutes = parseMinutes(tokens[0]);
    	WorklogInputBuilder builder = new WorklogInputBuilder(issue.getSelf());
    	builder.setAdjustEstimateAuto();
    	builder.setMinutesSpent(minutes);

    	if (tokens.length == 2) {
    		String comment = tokens[1];
    		builder.setComment(comment);
    	}
    	
    	try {
    		restClient.getIssueClient().addWorklog(issue.getWorklogUri(), builder.build()).claim();
    	} catch (RestClientException e) {
			throw new TrackerException(e);
		}
	}
	
	public User getUser(String username) {
		try {
			return restClient.getUserClient().getUser(username).claim();
		} catch (RestClientException e) {
			throw new TrackerException(e);
		}
	}
	
	private Collection<FieldInput> mapToFieldInputs(String projectKey, Map<String, String> attributes) {
		List<FieldInput> list = Lists.newArrayList();
		
		if (attributes.containsKey(JiraConstants.SUMMARY)) {
			list.add(FieldInputHelper.toSummary(getString(attributes, JiraConstants.SUMMARY)));
		}
		
		if (attributes.containsKey(JiraConstants.ISSUE_TYPE)) {
			IssueType type = getRemoteIssueType(projectKey, getString(attributes, JiraConstants.ISSUE_TYPE));
			list.add(FieldInputHelper.toIssueType(type.getId()));
		}
		
		if (attributes.containsKey(JiraConstants.DESCRIPTION)) {
			list.add(FieldInputHelper.toDescription(getString(attributes, JiraConstants.DESCRIPTION)));
		}
		
		if (attributes.containsKey(JiraConstants.ASSIGNEE)) {
			String username = getString(attributes, JiraConstants.ASSIGNEE);
			list.add(FieldInputHelper.toAssignee(username));
		}
		
		if (attributes.containsKey(JiraConstants.REPORTER)) {
			String username = getString(attributes, JiraConstants.REPORTER);
			list.add(FieldInputHelper.toReporter(username));
		}
		
		if (attributes.containsKey(JiraConstants.STATUS)) {
			String status = getString(attributes, JiraConstants.STATUS);
			Status s = getRemoteStatus(status);
			list.add(FieldInputHelper.toStatus(s.getId()));
		}
		
		if (attributes.containsKey(JiraConstants.PRIORITY)) {
			String p = getString(attributes, JiraConstants.PRIORITY);
			Priority priority = getRemotePriority(p);
			list.add(FieldInputHelper.toPriority(priority.getId()));
		}
		
		if (attributes.containsKey(JiraConstants.RESOLUTION)) {
			String r = getString(attributes, JiraConstants.RESOLUTION);
			Resolution resolution = getRemoteResolution(r);
			list.add(FieldInputHelper.toResolution(resolution.getId()));
		}
		
		if (attributes.containsKey(JiraConstants.COMPONENTS)) {
			List<String> names = getList(attributes, JiraConstants.COMPONENTS);
			list.add(FieldInputHelper.toComponents(names));
		}
		
		if (attributes.containsKey(JiraConstants.AFFECTED_VERSIONS)) {
			List<String> versions = getList(attributes, JiraConstants.AFFECTED_VERSIONS);
			list.add(FieldInputHelper.toAffectVersions(versions));
		}
		
		if (attributes.containsKey(JiraConstants.FIX_FOR_VERSIONS)) {
			List<String> versions = getList(attributes, JiraConstants.FIX_FOR_VERSIONS);
			list.add(FieldInputHelper.toFixVersions(versions));
		}
		
		if (attributes.containsKey(JiraConstants.DUEDATE)) {
			String value = getString(attributes, JiraConstants.DUEDATE);
			String dateFormat = getString(attributes, JiraConstants.DATE_FORMAT);
			if (StringUtils.isEmpty(dateFormat)) {
				dateFormat = "yyyy-MM-dd";
			}
			
			Date date = null;
			try {
				date = DateUtils.parseDate(value, new String[] { dateFormat});
			} catch (DateParseException e) {
				e.printStackTrace();
			}
			
			if (date != null) {
				list.add(FieldInputHelper.toDueDate(date));
			}
		}
		
		Map<String, String> customs = getCustomFieldMap(attributes);
		if (!customs.isEmpty()) {
			List<Field> fields = getRemoteCustomFields();
			for (Entry<String, String> each : customs.entrySet()) {
				Field field = findCustomField(each.getKey(), fields);
				if (field == null)
					continue;
				
				String typeName = field.getSchema().getType().toUpperCase();
				Object value = each.getValue();
				try {
					FieldSchemaType type = FieldSchemaType.valueOf(typeName);
					switch (type) {
					case NUMBER:
						value = Double.valueOf(each.getValue());
						break;
						
					default:
						break;
					}
				} catch (IllegalArgumentException e) {
					logger.error("Unknown custom field type " + typeName);
				}
				
				list.add(new FieldInput(field.getId(), value));
			}
		}
		
		return list;
	}
	
	private static enum FieldSchemaType {
		NUMBER,
		STRING,
		DATE,
		DATETIME
	}
	
	private Field findCustomField(String id, List<Field> list) {
		for (Field each : list) {
			String name = id.substring("customfield_".length());
			if (each.getId().equalsIgnoreCase(id) || each.getName().equalsIgnoreCase(name)) {
				return each;
			}
		}
		
		return null;
	}
	
	
	private Map<String, String> getCustomFieldMap(Map<String, String> attributes) {
		Map<String, String> map = Maps.newHashMap();
		for (Entry<String, String> each : attributes.entrySet()) {
			if (each.getKey().startsWith("customfield_")) {
				map.put(each.getKey(), each.getValue());
			}
		}
		
		return map;
	}
	
	protected List<String> getList(final Map<String, String> attributes, final String name) {
		String str = getString(attributes, name);
		if (Strings.isNullOrEmpty(str))
			return Collections.emptyList();
		
		return Lists.newArrayList(Splitter.on(',').trimResults().omitEmptyStrings().split(str));
	}
	
	protected String getString(final Map<String, String> attributes, final String name) {
    	return attributes.get(name);
    }
    
    protected String getRequiredString(final Map<String, String> attributes, final String name) {
    	String value = getString(attributes, name);
    	
    	if (StringUtils.isEmpty(value)) {
    		throw new TrackerException("The required string [" + name + "] does not specified!");
    	}
    	
    	return value;
    }
}
